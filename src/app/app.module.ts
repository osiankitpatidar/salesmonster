import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import component
import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { WelcomeComponent } from './welcome/welcome.component'
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfirmComponent } from './public/confirm/confirm.component'
// import services
import { AwsUtil } from "./service/aws.service";
import { UserRegistrationService, CognitoUtil, UserLoginService, 
         CognitoUpdateService } from './service/cognito.service';
import { DynamoDBService } from './service/ddb.service';
import { ChangePasswordComponent } from './private/change-password/change-password.component';
import { ForgetPasswordComponent } from './public/forget-password/forget-password.component';
import { LoginComponent } from './public/login/login.component';
import { SignupComponent } from './public/signup/signup.component';
import { CompanyRegistrationComponent } from './public/company-registration/company-registration.component';
import { SettingComponent } from './dashboard/setting/setting.component';
import { HeaderComponent } from './shared/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    DashboardComponent,
    ConfirmComponent,
    ChangePasswordComponent,
    ForgetPasswordComponent,
    LoginComponent,
    SignupComponent,
    CompanyRegistrationComponent,
    SettingComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [
   UserLoginService, AwsUtil, DynamoDBService, UserRegistrationService, CognitoUtil, CognitoUpdateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
