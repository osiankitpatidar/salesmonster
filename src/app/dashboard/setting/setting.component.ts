import { Component, OnInit } from '@angular/core';
import { UserLoginService, CognitoUpdateService, Callback } from "../../service/cognito.service";
import { Router } from "@angular/router";

declare var toastr: any;

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit, Callback {
  public user_object = {};
  constructor(public router: Router, public userService: UserLoginService, 
              public updateService: CognitoUpdateService){
    this.userService.getUserParameters(this);
    toastr.options = { "progressBar": true}
  }
  ngOnInit(){}
  callback(){}
  // call back for getUserParameters
  callbackWithParam(result: any){
    for (var i = 0; i < result.length; i++) {
      this.user_object[result[i].Name] = result[i].Value;
    }
  }

  update_user(username, email){
    this.updateService.updateUserParameter(username.value, email.value, this);
  }

  // call back for update user data
  cognitoCallback(message: string, result: any){
    message!=null ? toastr.success(message) : toastr.success('update successfully');
    this.router.navigate(['/dashboard']);
  }
}
