import {Component, OnInit } from '@angular/core';
import {LoggedInCallback, UserLoginService} from "../service/cognito.service";
import {Router} from "@angular/router";

declare var toastr: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  constructor(public router: Router, public userService: UserLoginService){
    this.userService.isAuthenticated(this);
    toastr.options = { "progressBar": true }
  }

  ngOnInit(){
    toastr.success('Welcome');
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    // console.log('isLoggedIn:' + message)
      if (!isLoggedIn) {
          this.router.navigate(['']);
      }
  }
}