import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './public/login/login.component';
import { SignupComponent } from './public/signup/signup.component';
import { ConfirmComponent } from './public/confirm/confirm.component';
import { ForgetPasswordComponent } from './public/forget-password/forget-password.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompanyRegistrationComponent } from './public/company-registration/company-registration.component';
import { SettingComponent } from './dashboard/setting/setting.component';

const homeRoutes: Routes = [
    {path: '', component: WelcomeComponent, pathMatch: 'full'},
    // { path: 'welcome', component: WelcomeComponent,
    //     // children:[
    //     //     {path:'login', component: LoginComponent}
    //     // ]
    // },
    {path:'forgetPassword', component: ForgetPasswordComponent},
    {path:'login', component: LoginComponent },
    {path:'signup', component: SignupComponent },
    {path:'dashboard', component: DashboardComponent },
    {path:'dashboard/setting', component: SettingComponent },
    {path:'confirm/:username', component: ConfirmComponent },
    {path:'company-registration', component: CompanyRegistrationComponent },
    {path: '**', redirectTo: '' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(homeRoutes);