import {Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserLoginService} from "../../service/cognito.service";
import {DynamoDBService} from "../../service/ddb.service";

declare var jQuery: any;
declare var waitingDialog: any;
declare var toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  errorMessage: string;
  show = 'Show';

   constructor(public userService: UserLoginService, public router: Router,  public ddb: DynamoDBService){
      toastr.options = { "progressBar": true, "positionClass": "toast-top-left", }
   }

  ngOnInit() {
    this.userService.isAuthenticated(this);
  }

    // login method
  onLogin(){
    if (this.email == null || this.password == null) {
        toastr.error('All fields are required');
        return;
    }
    this.errorMessage = null;
    this.userService.authenticate(this.email, this.password, this);
    waitingDialog.show('Please wait');
  }

  // call back for login
  cognitoCallback_login(message: string, result: any){
    if (message != null) { //error
        waitingDialog.hide();
        toastr.error(message);
        if (message === 'User is not confirmed.') {
           this.router.navigate(['/confirm', this.email])
        }
    }else{ //success
        waitingDialog.hide();
        this.ddb.writeLogEntry("login");
        this.router.navigate(['/dashboard']);
    }
  }
  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }
  
  showPassword(input: any){
    input.type = input.type === 'password' ? 'text' : 'password';
    this.show = this.show === 'Show' ? 'Hide' : 'Show';
  }
}