import {Component, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {UserRegistrationService, UserLoginService} from "../../service/cognito.service";

declare var toastr: any;
declare var waitingDialog: any

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnDestroy{
  confirmationCode: string;
  email: string;
  errorMessage: string;
  private sub: any;

  constructor(public regService: UserRegistrationService, public userService: UserLoginService, public router: Router, 
    public route: ActivatedRoute) {
    toastr.options = { "progressBar": true, "positionClass": "toast-top-left", }
    this.userService.isAuthenticated(this);
  }

  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }

  ngOnInit(){
      this.sub = this.route.params.subscribe(params => {
          this.email = params['username'];
      });
      this.errorMessage = null;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onConfirmRegistration() {
    this.regService.confirmRegistration(this.email, this.confirmationCode, this);
  }
  
  cognitoCallback_confirm_code(message: string, result: any) {
      if (message != null) {
          toastr.error(message);
      } else {
        let userDetail = JSON.parse(localStorage.getItem('user_detail'));
        console.log('userDetail', userDetail);
        this.userService.authenticate(userDetail.email, userDetail.password, this);
        // this.router.navigate(['/company-registration']);
      }
  }

  cognitoCallback_login(message: string, result: any){
    if (message != null) { //error
        waitingDialog.hide();
        toastr.error(message);
        if (message === 'User is not confirmed.') {
           this.router.navigate(['/confirm', this.email])
        }
    }else{ //success
        waitingDialog.hide();
        // localStorage.removeItem('user_detail');
        // this.ddb.writeLogEntry("login");
        /*this.router.navigate(['/dashboard']);*/
        
        this.router.navigate(['/company-registration'])
    }
  }
}