import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserRegistrationService, UserLoginService} from "../../service/cognito.service";
import {DynamoDBService} from "../../service/ddb.service";

declare var waitingDialog: any;
declare var toastr: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  name: string;
  email: string;
  password: string;
  show = 'Show';

  constructor(public userRegistration: UserRegistrationService, public router: Router, 
    public ddb: DynamoDBService, public userService: UserLoginService){ 
    toastr.options = { "progressBar": true , "positionClass": "toast-top-left"}
  }

  ngOnInit() {
    this.userService.isAuthenticated(this);
  }

  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }

  // registration method
  onRegister(){
    let registrationUser = { email: this.email, password: this.password, name: this.name}
    this.userRegistration.register(registrationUser, this);
    localStorage.setItem("user_detail", JSON.stringify(registrationUser));
  }

  // call back for signUp
  cognitoCallback(message: string, result: any){
    if (message != null) { //error
        toastr.error(message);
    }else{ //success
        this.router.navigate(['/confirm', this.email])
    }
  }
  
  showPassword(input: any){
    input.type = input.type === 'password' ? 'text' : 'password';
    this.show = this.show === 'Show' ? 'Hide' : 'Show';
  }
}
