import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { UserRegistrationService, UserLoginService } from "../../service/cognito.service";
import { DynamoDBService } from "../../service/ddb.service";

@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.css']
})
export class CompanyRegistrationComponent implements OnInit {
  constructor(public userService: UserLoginService, public router: Router, 
              public location: Location, public ddb: DynamoDBService){
    this.userService.isAuthenticated(this);
  }

  isLoggedIn(message: string, isLoggedIn: boolean){
    if(!isLoggedIn){
      this.location.back();
    }
  }
  
  employees = [
     {value: '10', employee:'1 to 10'},
     {value: '25', employee:'upto 25'},
     {value: '50', employee:'upto 50'},
     {value: '100', employee:'upto 100'},
     {value: '101', employee:'more than 100'},
   ]

   Organization = [
     {value: 'it_services', name:'TI Services'},
     {value: 'software', name:'Software'},
     {value: 'banking', name:'Banking'},
     {value: 'other', name:'Other'},
   ]

  ngOnInit(){}

  create_company(name:any, size:any, category:string){
    let org_name : string = this.convertToSlug(name)
    this.ddb.writeCompany(org_name,size,category)
  }

  convertToSlug(Text:any){
      return Text
          .toLowerCase()
          .replace(/[^\w ]+/g,'')
          .replace(/ +/g,'-')
          ;
  }

  onBack(){
    this.location.back();
  }
}