import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {Location} from '@angular/common';
import { UserLoginService, CognitoCallback } from '../../service/cognito.service' ;

declare var jQuery: any;
declare var toastr: any;

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  verificationCode: string;
  email: string;
  password: string;
  errorMessage: string;
  showResetPassword: boolean;
  constructor(public userService: UserLoginService, public router: Router, public location: Location){
    toastr.options ={ 'progressBar': true, 'positionClass':'toast-top-left' }
  }

  ngOnInit(){
  }
  
  //call for forgot password
  onNext(){
    if (this.email==null) {
      toastr.error('Enter email id')
      return
    }
    this.userService.forgotPassword(this.email, this);
  }

  onBack(){
    this.location.back();
  }

  cognitoCallback(message: string, result: any){
    if(message!=null){
      toastr.error(message);
    }else {
      this.showResetPassword = true;
    }
  }

  onConfirmNewPassword(){
    if (this.verificationCode==null || this.password == null) { 
      return toastr.error('Verification Code & Password required')
    }
    this.userService.confirmNewPassword(this.email, this.password, this.verificationCode, this);
  }
  
  cognitoCallback_confirm_code(message: string, result: string){
    if(message!=null) {
      toastr.error(message);
    }else {
      this.router.navigate(['/login']);
    }
  }
}