import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {UserLoginService} from "../service/cognito.service";

declare var toastr: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
})
export class WelcomeComponent {
  constructor(public userService: UserLoginService, 
              public router: Router){
    toastr.options = { "progressBar": true, 'positionClass': 'toast-top-left' }
  }

  ngOnInit(){
    this.userService.isAuthenticated(this);
    toastr.success('Welcome on salesmonster');
  }

  // check user is loggedin or not
  isLoggedIn(message: string, isLoggedIn: boolean) {
    if(isLoggedIn){
      this.router.navigate(['/dashboard']);
    }
  }
}