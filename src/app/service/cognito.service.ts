import {Injectable, Inject} from '@angular/core';
import {environment} from "../../environments/environment";
import {DynamoDBService} from "./ddb.service";

declare var AWSCognito: any;
declare var AWS: any;

export interface CognitoCallback {
    cognitoCallback(message: string, result: any): void;
}

export interface CognitoCallback_Confirm_Code {
    cognitoCallback_confirm_code(message: string, result: any): void;
}

export interface CognitoCallback_Login {
    cognitoCallback_login(message: string, result: any): void;
}

export interface LoggedInCallback {
    isLoggedIn(message: string, loggedIn: boolean): void;
}

export interface Callback {
    callback(): void;
    callbackWithParam(result: any): void;
}

@Injectable()
export class CognitoUtil{
    public static _REGION = environment.region;
    public static _IDENTITY_POOL_ID = environment.identityPoolId;
    public static _USER_POOL_ID = environment.userPoolId;
    public static _CLIENT_ID = environment.clientId;
    public static _POOL_DATA = {
        UserPoolId: CognitoUtil._USER_POOL_ID,
        ClientId: CognitoUtil._CLIENT_ID
    };

    public static getAwsCognito(): any {
        return AWSCognito
    }

    getUserPool(){
        return new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(CognitoUtil._POOL_DATA);
    }

    getCurrentUser() {
        return this.getUserPool().getCurrentUser();
    }
    getCognitoIdentity(): string {
        return AWS.config.credentials.identityId;
    }

    getAccessToken(callback: Callback): void {
        if (callback == null) {
            throw("CognitoUtil: callback in getAccessToken is null...returning");
        }
        if (this.getCurrentUser() != null)
            this.getCurrentUser().getSession(function (err, session) {
                if (err) {
                    console.log("CognitoUtil: Can't set the credentials:" + err);
                    callback.callbackWithParam(null);
                }

                else {
                    if (session.isValid()) {
                        callback.callbackWithParam(session.getAccessToken().getJwtToken());
                    }
                }
            });
        else
            callback.callbackWithParam(null);
    }

    getIdToken(callback: Callback): void {
        if (callback == null) {
            throw("CognitoUtil: callback in getIdToken is null...returning");
        }
        if (this.getCurrentUser() != null)
            this.getCurrentUser().getSession(function (err, session) {
                if (err) {
                    console.log("CognitoUtil: Can't set the credentials:" + err);
                    callback.callbackWithParam(null);
                }
                else {
                    if (session.isValid()) {
                        callback.callbackWithParam(session.getIdToken().getJwtToken());
                    } else {
                        console.log("CognitoUtil: Got the id token, but the session isn't valid");
                    }
                }
            });
        else
            callback.callbackWithParam(null);
    }

    getRefreshToken(callback: Callback): void {
        if (callback == null) {
            throw("CognitoUtil: callback in getRefreshToken is null...returning");
        }
        if (this.getCurrentUser() != null)
            this.getCurrentUser().getSession(function (err, session) {
                if (err) {
                    console.log("CognitoUtil: Can't set the credentials:" + err);
                    callback.callbackWithParam(null);
                }

                else {
                    if (session.isValid()) {
                        callback.callbackWithParam(session.getRefreshToken());
                    }
                }
            });
        else
            callback.callbackWithParam(null);
    }

    refresh(): void {
        this.getCurrentUser().getSession(function (err, session) {
            if (err) {
                console.log("CognitoUtil: Can't set the credentials:" + err);
            }

            else {
                if (session.isValid()) {
                    console.log("CognitoUtil: refreshed successfully");
                } else {
                    console.log("CognitoUtil: refreshed but session is still not valid");
                }
            }
        });
    }
}

@Injectable()
export class UserRegistrationService {

    constructor(@Inject(CognitoUtil) public cognitoUtil: CognitoUtil){}

    register(user, callback: CognitoCallback): void {
      console.log("UserRegistrationService: user is " + user);

      let attributeList = [];

      let dataEmail = {
          Name: 'email',
          Value: user.email
      };
      let dataNickname = {
          Name: 'nickname',
          Value: user.name
      };
      attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail));
      attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataNickname));

      this.cognitoUtil.getUserPool().signUp(user.email, user.password, attributeList, null, function (err, result) {
          if (err) {
              callback.cognitoCallback(err.message, null);
          } else {
              console.log("UserRegistrationService: registered user is " + result);
              callback.cognitoCallback(null, result);
          }
      });
    }

    // Confirming a registered, unauthenticated user using a confirmation code received via SMS
    confirmRegistration(username: string, confirmationCode: string, callback: CognitoCallback_Confirm_Code): void {
        let userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        console.log("confirmRegistration userData:"+ userData);
        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        console.log("confirmRegistration cognitoUser:"+ cognitoUser);

        cognitoUser.confirmRegistration(confirmationCode, true, function (err, result) {
            if (err) {
                callback.cognitoCallback_confirm_code(err.message, null);
            }else{
                callback.cognitoCallback_confirm_code(null, result);
            }
        });
    }
}

@Injectable()
export class UserLoginService{

  constructor(public ddb: DynamoDBService, public cognitoUtil: CognitoUtil){}

    authenticate(username: string, password: string, callback: CognitoCallback_Login) {

        console.log("UserLoginService: starting the authentication")
        AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})
        // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
        let authenticationData = {
            Username: username,
            Password: password,
        };
        let authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);

        let userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        console.log("UserLoginService: Params set...Authenticating the user");
        let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log("check authenticated user");
                // Change the key below according to the specific region your user pool is in.
                var logins = {}
                logins['cognito-idp.' + CognitoUtil._REGION + '.amazonaws.com/' + CognitoUtil._USER_POOL_ID] = result.getIdToken().getJwtToken();

                // Add the User's Id Token to the Cognito credentials login map.
                AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                    IdentityPoolId: CognitoUtil._IDENTITY_POOL_ID,
                    Logins: logins
                });

                console.log("UserLoginService: set the AWS credentials - " + JSON.stringify(AWS.config.credentials));

                console.log("UserLoginService: set the AWSCognito credentials - " + JSON.stringify(AWSCognito.config.credentials));           
                AWS.config.credentials.get(function (err) {
                    console.log("Authenticate ERROR: "+ err)
                    if (!err) {
                        callback.cognitoCallback_login(null, result);
                    } else {
                        console.log('Message:'+ err.message);
                        callback.cognitoCallback_login(err.message, null);
                    }
                });
            },
            onFailure: function (err) {
                callback.cognitoCallback_login(err.message, null);
            },
        });
    }

    // before & after check user is authenticated or user session 
    isAuthenticated(callback: LoggedInCallback) {
        if (callback == null)
            throw("UserLoginService: Callback in isAuthenticated() cannot be null");

        let cognitoUser = this.cognitoUtil.getCurrentUser();

        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    console.log("UserLoginService: Couldn't get the session: " + err, err.stack);
                    callback.isLoggedIn(err, false);
                }
                else {
                    console.log("UserLoginService: Session is " + session.isValid());
                    callback.isLoggedIn(err, session.isValid());
                }
            });
        } else {
            console.log("UserLoginService: can't retrieve the current user");
            callback.isLoggedIn("Can't retrieve the CurrentUser", false);
        }
    }

    // after login change password authorized user
    changePassword(oldPassword: string, newPassword: string, callback: CognitoCallback){
      let cognitoUser = this.cognitoUtil.getCurrentUser();
      if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
              if (err) {
                  console.log("UserLoginService: Couldn't get the session: " + err, err.stack);
              }
              else{
                cognitoUser.changePassword(oldPassword, newPassword, function(err, result) {
                  if (err) {
                    callback.cognitoCallback(err.message, null); 
                  }else{
                    callback.cognitoCallback(null, result);
                  }
                });
              }
            });
        }
    }

    forgotPassword(email: string, callback: CognitoCallback){
      let userData = {
        Username: email,
        Pool: this.cognitoUtil.getUserPool()
      };
      let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
      cognitoUser.forgotPassword({
        onSuccess: function (result) {
          callback.cognitoCallback(null, result);
        },
        onFailure: function (err) {
            callback.cognitoCallback(err.message, null);
        }
      });
    }

    confirmNewPassword(email: string, password: string, verificationCode: string, callback: CognitoCallback_Confirm_Code){
       let userData = {
         Username: email,
         Pool: this.cognitoUtil.getUserPool()
       }
       let cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
       cognitoUser.confirmPassword(verificationCode, password, {
         onSuccess: function(result){
           callback.cognitoCallback_confirm_code(null, result);
         },
         onFailure: function(err){
           callback.cognitoCallback_confirm_code(err.message, null);
         }
       });
    }

    logout(){
      this.ddb.writeLogEntry("logout");
      this.cognitoUtil.getCurrentUser().signOut();
      AWS.config.credentials.clearCachedId();
    }

    // get parameters authenticated user
    getUserParameters(callback: Callback){
        let cognitoUser = this.cognitoUtil.getCurrentUser();
        if (cognitoUser!=null) {
          cognitoUser.getSession(function (err, session) {
            if (err) {
              console.log("UserParametersService: Couldn't retrieve the user");
            }else{
              cognitoUser.getUserAttributes(function (err, result) {
                if (err) {
                    console.log("UserParametersService: in getParameters: " + err);
                } else {
                    callback.callbackWithParam(result);
                }
              });
            }
          });
        }else{
          callback.callbackWithParam(null)
        }
    }
}

@Injectable()
export class CognitoUpdateService {
  constructor(public cognitoUtil: CognitoUtil){}
  
  updateUserParameter(username, email, callback: CognitoCallback){
    let cognitoUser = this.cognitoUtil.getCurrentUser();
    if (cognitoUser!=null) {
      cognitoUser.getSession(function (err, session) {
        if (err) {
          console.log("UserParametersService: Couldn't retrieve the user");
        }
        else
        {
          let attributeList = [];
          // let dataEmail = { Name: 'email', Value: email };
          let dataNickname = { Name: 'nickname', Value: username };
          // attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail));
          attributeList.push(new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataNickname));
          cognitoUser.updateAttributes(attributeList, function(err, result) {
            if (err) { // return err
              callback.cognitoCallback(err, null);
            }else{ // return SUCCESS
              callback.cognitoCallback(null, result);
            }
          });
        };
      });
    }
  }
}