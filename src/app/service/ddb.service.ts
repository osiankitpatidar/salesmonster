import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";

declare var AWS: any;

@Injectable()
export class DynamoDBService {

    constructor() {
        console.log("DynamoDBService: constructor");
    }

    getAWS() {
        return AWS;
    }

    writeLogEntry(type: string) {
        try {
            let date = new Date().toString();
            console.log("DynamoDBService: Writing log entry. Type:" + type + " ID: " + AWS.config.credentials.params.IdentityId + " Date: " + date);
            this.write(AWS.config.credentials.params.IdentityId, date, type);
        } catch (exc) {
            console.log("DynamoDBService: Couldn't write to DDB");
        }
    }

    write(data: string, date: string, type: string): void {
        console.log("DynamoDBService: writing " + type + " entry");
        var DDB = new AWS.DynamoDB({
            params: {TableName: environment.ddbTableName}
        });
        // Write the item to the table
        var itemParams =
            {
                Item: {
                    userId: {S: data},
                    activityDate: {S: date},
                    type: {S: type}
                }
            };
        DDB.putItem(itemParams, function (result) {
            console.log("DynamoDBService: wrote entry: " + JSON.stringify(result));
        });
    }

    writeCompany(name: any, size: number, category: string){
        try{
            var DDB = new AWS.DynamoDB({
                params: {TableName:'organization'}
                });
            var itemParams =
                {
                    Item: {
                        userId: {S: AWS.config.credentials.params.IdentityId},
                        name: {S: name},
                        size: {S: size},
                        category: {S: category}
                    }
                };

            DDB.putItem(itemParams, function (result) {
                console.log("DynamoDBService: wrote entry: " + JSON.stringify(result));
            });



        }catch (exc) {
            console.log("DynamoDBService: Couldn't write to DDB"+ exc);
        }
    }
}