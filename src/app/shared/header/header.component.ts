import { Component, OnInit } from '@angular/core';
import { UserLoginService, Callback } from "../../service/cognito.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user_object = {};
  constructor(public router: Router, public userService: UserLoginService) { 
      this.userService.getUserParameters(this);
  }
  ngOnInit(){}
  
  callback(){}

  // call back for getUserParameters
  callbackWithParam(result: any){
    for (let i = 0; i < result.length; i++){ 
      this.user_object[result[i].Name] = result[i].Value; 
    }
  }
  
  onLogout(){
    this.userService.logout();
    this.router.navigate(['']);
  }
}