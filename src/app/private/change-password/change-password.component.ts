import { Component, OnInit } from '@angular/core';
import { UserLoginService } from '../../service/cognito.service' ;
declare var jQuery: any;
declare var toastr: any;
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {  
  newPassword: string;
  oldPassword: string;
  errorMessage: string;
  constructor(public userLoginService: UserLoginService){
    toastr.options = { "progressBar": true}
  }
  ngOnInit(){}

  change_password(){
    this.userLoginService.changePassword(this.oldPassword, this.newPassword, this);
  }

  cognitoCallback(message: string, result: string){
      message!=null ? toastr.success(message) : toastr.success(result);
      if (message==null) {
        this.newPassword = ''; this.oldPassword = '';
      }
  }
}