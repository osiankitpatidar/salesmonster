// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
/*  region: 'us-east-1',
  identityPoolId: 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
  userPoolId: 'us-east-1_PGSbCVZ7S',
  clientId: 'hh5ibv67so0qukt55c5ulaltk',

  rekognitionBucket: 'rekognition-pics',
  albumName: "usercontent",
  bucketRegion: 'us-east-1',
  ddbTableName: 'LoginTrail'*/
  
  region: 'us-west-2',
  identityPoolId: 'us-west-2:78b08530-0121-4f8e-9cea-05f72326ef72',
  userPoolId: 'us-west-2_DZH7aTJUY',
  clientId: 'f2vaj24au4qiup1nu6ttjfajn',

  rekognitionBucket: 'rekognition-pics',
  albumName: "usercontent",
  bucketRegion: '',
  ddbTableName: 'user_login_log'
};
