import { AwsAngular2Page } from './app.po';

describe('aws-angular2 App', () => {
  let page: AwsAngular2Page;

  beforeEach(() => {
    page = new AwsAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
